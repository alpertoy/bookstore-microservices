# Bookstore in Netflix OSS

![model](diagram.png)

## How to test?

Run services by priority number and make sure link is open before going for next

1. <a href="https://naming-server-alper.herokuapp.com/" target="_blank">Eureka Server</a>

2. <a href="https://currency-service-alper.herokuapp.com/swagger-ui/index.html?configUrl=/currency-service/v3/api-docs/swagger-config#/" target="_blank">Currency Service</a>

3. <a href="https://book-service-alper.herokuapp.com/swagger-ui/index.html?configUrl=/book-service/v3/api-docs/swagger-config#/" target="_blank">Book Service</a>

4. <a href="https://api-gateway-alper.herokuapp.com/webjars/swagger-ui/index.html?configUrl=%2Fv3%2Fapi-docs%2Fswagger-config&urls.primaryName=book-service#/" target="_blank">API Gateway</a>

And refresh Eureka Server page to see services are registered to server.

## Note 
For API Gateway Swagger Doc (You can switch services from top right "select definition" section) may throw error such as " service unavailable ". It is just a bug, avoid it and refresh the page.

Still not working? Try to send a GET request via link below and try to open API Gateway's swagger page :) as I said, it is just a bug not related to the services.

`https://api-gateway-alper.herokuapp.com/book-service/5/GBP`

## Avaiable Data in Book Service

```javascript
    INSERT INTO `book` (`author`, `launch_date`, `price`, `title`) VALUES
	('Michael C. Feathers', '2017-11-29 13:50:05.878000', 8.57, 'Working effectively with legacy code'),
	('Ralph Johnson, Erich Gamma, John Vlissides and Richard Helm', '2017-11-29 15:15:13.636000', 7.87, 'Design Patterns'),
	('Robert C. Martin', '2009-01-10 00:00:00.000000', 13.46, 'Clean Code'),
	('Crockford', '2017-11-07 15:09:01.674000', 11.71, 'JavaScript'),
	('Steve McConnell', '2017-11-07 15:09:01.674000', 10.14, 'Code complete'),
	('Martin Fowler and Kent Beck', '2017-11-07 15:09:01.674000', 15.38, 'Refactoring'),
	('Eric Freeman, Elisabeth Freeman, Kathy Sierra, Bert Bates', '2017-11-07 15:09:01.674000', 19.23, 'Head First Design Patterns'),
	('Eric Evans', '2017-11-07 15:09:01.674000', 16.09, 'Domain Driven Design'),
	('Brian Goetz and Tim Peierls', '2017-11-07 15:09:01.674000', 13.99, 'Java Concurrency in Practice');
```

## Available Data in Currency Service

```javascript
    INSERT INTO `currency` (`from_currency`, `to_currency`, `conversion_factor`) VALUES 
    ('USD', 'EUR', 0.82),
    ('USD', 'GBP', 0.70),
    ('USD', 'AUD', 1.29),
    ('USD', 'CAD', 1.20),
    ('USD', 'JPY', 109.84);
```






